/*
ks4lookup will output the ID matching the given points in the K-S database.
*/

package main

import (
	"bitbucket.org/pcas/ks4binary/ks4"
	"bitbucket.org/pcas/ks4binary/point"
	"bitbucket.org/pcastools/flag"
	"errors"
	"fmt"
	"os"
)

// Options describes the options.
type Options struct {
	DB        string
	InverseDB string
	Pts       []point.Points
}

// The default values.
const (
	DefaultDB        = "ks4.db"
	DefaultInverseDB = "ks4inv.db"
)

// Name is the name of the executable.
const Name = "ks4lookup"

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := &Options{
		DB:        DefaultDB,
		InverseDB: DefaultInverseDB,
	}
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s looks up the IDs for points in the 4-dimensional Kreuzer-Skarke database.\n\nUsage: %s [options] verts1 [... vertsN]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("db", &opts.DB, opts.DB, "The database", ""),
		flag.String("inverse-db", &opts.InverseDB, opts.InverseDB, "The inverse database", ""),
	)
	// Parse the flags
	flag.Parse()
	// Extract the points
	pts := make([]point.Points, 0, flag.NArg())
	for i, s := range flag.Args() {
		p, err := point.ParsePoints(s)
		if err != nil {
			return fmt.Errorf("unable to parse points %d: %w", i+1, err)
		}
		pts = append(pts, p)
	}
	opts.Pts = pts
	// Check that the databases to use are non-empty
	if len(opts.DB) == 0 {
		return errors.New("no database path is specified")
	} else if len(opts.InverseDB) == 0 {
		return errors.New("no inverse database path is specified")
	}
	// Looks good
	return nil
}

// lookupPoints prints the IDs for the given points in the database.
func lookupPoints(db *ks4.InverseDB, pts []point.Points) error {
	for _, p := range pts {
		id, err := db.Get(p)
		if err != nil {
			return err
		} else if id == 0 {
			fmt.Printf("unknown:%s\n", p)
		} else {
			fmt.Printf("%d:%s\n", id, p)
		}
	}
	return nil
}

// runMain executes the main function, returning any errors.
func runMain(opts *Options) error {
	// Open the database file
	r, err := os.Open(opts.DB)
	if err != nil {
		return err
	}
	defer r.Close()
	// Load the DB
	db, err := ks4.Read(r)
	if err != nil {
		return err
	}
	// Open the inverse database file
	ir, err := os.Open(opts.InverseDB)
	if err != nil {
		return err
	}
	defer ir.Close()
	// Load the inverse DB
	invdb, err := ks4.ReadInverseDB(ir, db)
	if err != nil {
		return err
	}
	// Perform the lookup
	return lookupPoints(invdb, opts.Pts)
}

// main is the program entry point.
func main() {
	assertNoErr(runMain(setOptions()))
}
