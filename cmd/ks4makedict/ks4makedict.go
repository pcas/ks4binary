/*
ks4makedict creates the dictionary file used to efficiently encode the binary data.

This is STEP 2 of 7.
*/

package main

import (
	"bitbucket.org/pcas/ks4binary/ks4"
	"bitbucket.org/pcas/ks4binary/point"
	"bitbucket.org/pcastools/flag"
	"bufio"
	"errors"
	"fmt"
	"github.com/c2h5oh/datasize"
	"github.com/gosuri/uiprogress"
	"github.com/olekukonko/tablewriter"
	"os"
	"runtime"
	"sort"
	"strconv"
)

// Options describes the options.
type Options struct {
	Raw     string
	NewRaw  string
	Dict    string
	NewDict string
}

// The default values.
const (
	DefaultRaw     = "ks4.raw.initial"
	DefaultNewRaw  = "ks4.raw"
	DefaultDict    = ""
	DefaultNewDict = "ks4.dict"
)

// progressBarInterval defines how frequently the progress bar is updated.
const progressBarInterval = 100000

// maxSize is how large we allow our k-points dictionary to be.
const maxSize = 500000

// Name is the name of the executable.
const Name = "ks4makedict"

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := &Options{
		Raw:     DefaultRaw,
		NewRaw:  DefaultNewRaw,
		Dict:    DefaultDict,
		NewDict: DefaultNewDict,
	}
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s generates a dictionary for the 4-dimensional Kreuzer-Skarke database.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("src-raw", &opts.Raw, opts.Raw, "The original raw data", ""),
		flag.String("src-dict", &opts.Dict, opts.Dict, "The original dictionary", ""),
		flag.String("dst-raw", &opts.NewRaw, opts.NewRaw, "The new raw data", ""),
		flag.String("dst-dict", &opts.NewDict, opts.NewDict, "The new dictionary", ""),
	)
	// Parse the flags
	flag.Parse()
	// Check that the paths are non-empty
	if len(opts.Raw) == 0 {
		return errors.New("no source raw path is specified")
	} else if len(opts.NewRaw) == 0 {
		return errors.New("no destination raw path is specified")
	} else if len(opts.NewDict) == 0 {
		return errors.New("no destination dictionary path is specified")
	}
	// Looks good
	return nil
}

// counterData is used to sort counter data in decreasing count order.
type counterData struct {
	pts    []point.Points
	counts []int
}

// Len returns the number of entries in the data.
func (t counterData) Len() int {
	return len(t.counts)
}

// Less returns true iff the i-th enty is >= the j-th entry.
func (t counterData) Less(i int, j int) bool {
	return t.counts[i] >= t.counts[j]
}

// Swap swaps the i-th and j-th entries.
func (t counterData) Swap(i int, j int) {
	t.counts[i], t.counts[j] = t.counts[j], t.counts[i]
	t.pts[i], t.pts[j] = t.pts[j], t.pts[i]
}

// chompPoints chomps off multiples of k vertices that are in the dictionary d. Returns the remaining points.
func chompPoints(pts point.Points, k int, d *point.Dict) point.Points {
	for len(pts) >= k {
		_, ok := d.IDForPoints(pts[:k])
		if !ok {
			return pts
		}
		pts = pts[k:]
	}
	return pts
}

// updatePoints returns a counter, a function to update the counter, and a cancel function, for length k points, using the given dictionary.
func updatePoints(k int, d *point.Dict, bar *uiprogress.Bar) (*point.Counter, func(int64, point.Points) error, func()) {
	// Create the counter
	c := point.NewCounter(1009, 4*maxSize)
	// Create the communication channels
	nworkers := 2 * runtime.NumCPU()
	ptsC := make(chan point.Points, 1000000)
	doneC := make(chan bool, nworkers)
	// Start the workers in a new go-routine
	for i := 0; i < nworkers; i++ {
		go func() {
			for pts := range ptsC {
				for i := 6; i >= k; i-- {
					pts = chompPoints(pts, i, d)
				}
				if len(pts) >= k {
					c.Increment(pts[:k])
				}
			}
			doneC <- true
		}()
	}
	// Return the counter, update function, and cancel function
	return c, func(id int64, pts point.Points) error {
			ptsC <- pts
			if id%progressBarInterval == 0 {
				bar.Incr()
			}
			return nil
		}, func() {
			close(ptsC)
			for i := 0; i < nworkers; i++ {
				<-doneC
			}
		}
}

// counterToTable outputs a table of common points along with their count.
func counterToTable(pts []point.Points, counts []int) {
	t := tablewriter.NewWriter(os.Stdout)
	t.SetBorder(false)
	t.SetHeader([]string{"count", "points"})
	t.SetColumnAlignment([]int{tablewriter.ALIGN_RIGHT, tablewriter.ALIGN_LEFT})
	if size := len(pts); size <= 10 {
		for i := 0; i < size; i++ {
			t.Append([]string{
				strconv.Itoa(counts[i]),
				pts[i].String(),
			})
		}
	} else {
		for i := 0; i < 10; i++ {
			t.Append([]string{
				strconv.Itoa(counts[i]),
				pts[i].String(),
			})
		}
		t.Append([]string{"...", "..."})
		t.Append([]string{
			strconv.Itoa(counts[size-1]),
			pts[size-1].String(),
		})
	}
	t.Render()
}

// addPointsToDict outputs the points in the given counter to the dictionary d, returning the new dictionary. All entries with counts smaller than n will be skipped.
func addPointsToDict(c *point.Counter, n int, d *point.Dict) (*point.Dict, error) {
	pts, counts := c.ToSlices(n)
	sort.Sort(counterData{
		pts:    pts,
		counts: counts,
	})
	if size := len(pts); size > maxSize {
		pts = pts[:maxSize]
		counts = counts[:maxSize]
	}
	fmt.Printf("Dictionary size: %d\n", len(pts))
	counterToTable(pts, counts)
	fmt.Println("Adding points to the encode dictionary...")
	return d.Append(pts...)
}

// createPointsDict creates and returns the dictionary on k points.
func createPointsDict(dbpath string, k int, d *point.Dict, decodedict *point.Dict) (*point.Dict, error) {
	// Provide some feedback
	fmt.Printf("Building %d-points dictionary...\n", k)
	// Open the database file
	r, err := os.Open(dbpath)
	if err != nil {
		return nil, fmt.Errorf("error opening source database: %w", err)
	}
	defer r.Close()
	br := bufio.NewReader(r)
	// Start the progress bar drawer running in its own go-routine
	prog := uiprogress.New()
	go prog.Start()
	// Create the progress bar
	bar := prog.AddBar(ks4.DBSize / progressBarInterval)
	bar.PrependCompleted()
	// Create the counter
	c, f, cancel := updatePoints(k, d, bar)
	err = ks4.ForEach(br, decodedict, f)
	cancel()
	// Stop the progress bar and handle any errors
	prog.Stop()
	if err != nil {
		return nil, err
	}
	// Add the points to the dictionary d
	n := k
	if n == 1 {
		n = 2
	}
	return addPointsToDict(c, n, d)
}

// createEncodeDict creates, writes-out, and returns the encode dictionary.
func createEncodeDict(dbpath string, encodepath string, decodedict *point.Dict) (*point.Dict, error) {
	// Create the dictionary
	var encodedict *point.Dict
	for i := 10; i > 0; i-- {
		var err error
		encodedict, err = createPointsDict(dbpath, i, encodedict, decodedict)
		if err != nil {
			return nil, err
		}
	}
	fmt.Printf("New dictionary created with %d entries.\n", encodedict.Len())
	// Open the encode dictionary file for writing. Any existing file will
	// be replaced.
	fh, err := os.Create(encodepath)
	if err != nil {
		return nil, err
	}
	defer fh.Close()
	// Wrap the encode dictionary file in a buffer
	bw := bufio.NewWriter(fh)
	defer bw.Flush()
	// Write out the dictionary
	n, err := encodedict.WriteTo(bw)
	if err != nil {
		return nil, err
	}
	fmt.Printf("Dictionary size: %s\n", datasize.ByteSize(n).HumanReadable())
	return encodedict, nil
}

// reencodeDatabase reencodes the database using the new encode dictionary.
func reencodeDatabase(dbpath string, newdbpath string, decodedict *point.Dict, encodedict *point.Dict) error {
	// Provide some feedback
	fmt.Println("Re-encoding database using new dictionary...")
	// Open the database file
	r, err := os.Open(dbpath)
	if err != nil {
		return err
	}
	defer r.Close()
	br := bufio.NewReader(r)
	// Open the output file for writing. Any existing file will be replaced.
	w, err := os.Create(newdbpath)
	if err != nil {
		return err
	}
	defer w.Close()
	// Wrap the output file in a buffer
	bw := bufio.NewWriter(w)
	defer bw.Flush()
	// Start the progress bar drawer running in its own go-routine
	prog := uiprogress.New()
	go prog.Start()
	// Create the progress bar
	bar := prog.AddBar(ks4.DBSize / progressBarInterval)
	bar.PrependCompleted()
	// Output the points
	var n int64
	err = ks4.ForEach(br, decodedict, func(id int64, pts point.Points) error {
		m, err := encodedict.EncodePoints(pts, bw)
		n += int64(m)
		if id%progressBarInterval == 0 {
			bar.Incr()
		}
		return err
	})
	// Stop the progress bar and handle any errors
	prog.Stop()
	if err != nil {
		return err
	}
	// Provide some feedback before returning
	fmt.Printf("Database size: %s\n", datasize.ByteSize(n).HumanReadable())
	return nil
}

// loadDict loads the dictionary from the given path.
func loadDict(path string) (*point.Dict, error) {
	// Is there anything to do?
	if len(path) == 0 {
		return nil, nil
	}
	// Open the file
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	// Read in the dictionary
	return point.ReadDict(bufio.NewReader(f))
}

// runMain executes the main function, returning any errors.
func runMain(opts *Options) error {
	// Read the dictionary
	decodedict, err := loadDict(opts.Dict)
	if err != nil {
		return err
	}
	// Create the new encode dictionary
	encodedict, err := createEncodeDict(opts.Raw, opts.NewDict, decodedict)
	if err != nil {
		return err
	}
	// Re-encode the database
	return reencodeDatabase(opts.Raw, opts.NewRaw, decodedict, encodedict)
}

// main is the program entry point.
func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	assertNoErr(runMain(setOptions()))
}
