/*
ks4init creates the initial binary version of the Kreuzer-Skarke database
from the plain text files.

This is STEP 1 of 7.
*/

package main

import (
	"bitbucket.org/pcas/ks4binary/ks4"
	"bitbucket.org/pcas/ks4binary/point"
	"bitbucket.org/pcastools/flag"
	"bufio"
	"errors"
	"fmt"
	"github.com/gosuri/uiprogress"
	"io"
	"os"
	"path/filepath"
)

// NumFiles is the number of plain text files that form the dataset.
const NumFiles = 47381

// Options describes the options.
type Options struct {
	Raw       string
	SourceDir string
}

// The default values.
const (
	DefaultRaw       = "ks4.raw.initial"
	DefaultSourceDir = "/data/fano/ks4"
)

// Name is the name of the executable.
const Name = "ks4init"

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := &Options{
		Raw:       DefaultRaw,
		SourceDir: DefaultSourceDir,
	}
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s create the initial 4-dimensional Kreuzer-Skarke database.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("raw", &opts.Raw, opts.Raw, "The output file", ""),
		flag.String("dir", &opts.SourceDir, opts.SourceDir, "The input directory", ""),
	)
	// Parse the flags
	flag.Parse()
	// Check that the database and source dir are non-empty
	if len(opts.Raw) == 0 {
		return errors.New("no output path is specified")
	} else if len(opts.SourceDir) == 0 {
		return errors.New("no input directory is specified")
	}
	// Looks good
	return nil
}

// parseFile parses the points from path, writing out their binary data to w, using the dictionary d (which may be nil). Returns the number of lines written.
func parseFile(path string, d *point.Dict, w io.Writer) (int64, error) {
	var n int64
	// Open the file
	fh, err := os.Open(path)
	if err != nil {
		return n, err
	}
	defer fh.Close()
	// Wrap the reader in a buffer
	br := bufio.NewReader(fh)
	// Parse the file
	for {
		// Read in the next line
		s, err := br.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				// We've reached the end of the file
				return n, nil
			}
			return n, err
		}
		// Parse the line to a slice of points
		pts, err := point.ParsePoints(s)
		if err != nil {
			return n, err
		}
		// Write out the binary data
		_, err = d.EncodePoints(pts, w)
		if err != nil {
			return n, err
		}
		n++
	}
}

// runMain executes the main function, returning any errors.
func runMain(opts *Options) error {
	// Open the output file for writing. Any existing file will be replaced.
	w, err := os.Create(opts.Raw)
	if err != nil {
		return err
	}
	defer w.Close()
	// Wrap the output file in a buffer
	bw := bufio.NewWriter(w)
	defer bw.Flush()
	// Start the progress bar drawer running in its own go-routine
	prog := uiprogress.New()
	go prog.Start()
	defer prog.Stop()
	// Create the progress bar
	bar := prog.AddBar(NumFiles)
	bar.PrependCompleted()
	// Start working through the input files
	var n int64
	for i := 1; i <= NumFiles; i++ {
		// Increment the progress bar
		bar.Incr()
		// Create the path to the input file
		path := filepath.Join(opts.SourceDir, fmt.Sprintf("%d.txt", i))
		// Parse the file
		m, err := parseFile(path, nil, bw)
		if err != nil {
			return fmt.Errorf("error parsing file %d: %w", i, err)
		}
		n += m
	}
	// Sanity check
	if n != ks4.DBSize {
		return fmt.Errorf("expected %d lines but found %d", ks4.DBSize, n)
	}
	return nil
}

// main is the program entry point.
func main() {
	assertNoErr(runMain(setOptions()))
}
