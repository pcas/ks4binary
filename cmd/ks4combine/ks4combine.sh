#!/bin/bash

###
# ks4combine combines the dictionary, index file, and raw binary data into
# a single file.
#
# This is STEP 4 of 7.
###

# Sanity check
if [ ! -e "ks4.dict" ]; then
    echo "unable to locate ks4.dict"
    exit 1
elif [ ! -e "ks4.idx" ]; then
    echo "unable to locate ks4.idx"
    exit 1
elif [ ! -e "ks4.raw" ]; then
    echo "unable to locate ks4.raw"
    exit 1
elif [ -e "ks4.db" ]; then
    echo "output file ks4.db already exists"
    exit 1
fi

# Combine the files
cat ks4.dict > ks4.db
cat ks4.idx >> ks4.db
cat ks4.raw >> ks4.db