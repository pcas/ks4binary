/*
ks4makeinverse creates the inverse look-up table of vertices (assumed to be in PALP normal form) to IDs.

This is STEP 5 of 7.
*/

package main

import (
	"bitbucket.org/pcas/ks4binary/ks4"
	"bitbucket.org/pcas/ks4binary/point"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/hash"
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/gosuri/uiprogress"
	"github.com/olekukonko/tablewriter"
	"io"
	"os"
	"sort"
	"strconv"
)

// Options describes the options.
type Options struct {
	DB     string
	Output string
}

// The default values.
const (
	DefaultDB     = "ks4.db"
	DefaultOutput = "ks4inv.raw"
)

// progressBarInterval defines how frequently the progress bar is updated.
const progressBarInterval = 100000

// Name is the name of the executable.
const Name = "ks4makeinverse"

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := &Options{
		DB:     DefaultDB,
		Output: DefaultOutput,
	}
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s creates the inverse look-up for the 4-dimensional Kreuzer-Skarke database.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("db", &opts.DB, opts.DB, "The database", ""),
		flag.String("out", &opts.Output, opts.Output, "The output", ""),
	)
	// Parse the flags
	flag.Parse()
	// Check that the paths are non-empty
	if len(opts.DB) == 0 {
		return errors.New("no database path is specified")
	} else if len(opts.Output) == 0 {
		return errors.New("no output path is specified")
	}
	// Looks good
	return nil
}

// buildInverseMap computes and returns the inverse hash map for hash values falling in the range min <= h <= max.
func buildInverseMap(db *ks4.DB, min hash.Value, max hash.Value) (map[hash.Value][]int64, error) {
	// Provide some output
	fmt.Printf("Building inverse for hashes in the range %d..%d...\n", min, max)
	// Start the progress bar drawer running in its own go-routine
	prog := uiprogress.New()
	go prog.Start()
	// Create the progress bar
	bar := prog.AddBar(ks4.DBSize / progressBarInterval)
	bar.PrependCompleted()
	// Build the inverse look-up map
	m := make(map[hash.Value][]int64)
	err := db.ForEach(func(id int64, pts point.Points) error {
		if h := pts.Hash(); h >= min && h <= max {
			m[h] = append(m[h], id)
		}
		if id%progressBarInterval == 0 {
			bar.Incr()
		}
		return nil
	})
	// Stop the progress bar and return
	prog.Stop()
	return m, err
}

// printInverseMapStats outputs statistics about the inverse map.
func printInverseMapStats(m map[hash.Value][]int64) {
	// Collect data about the number of hash collisions
	max := 0
	collisions := make(map[int]int64)
	for _, x := range m {
		n := len(x)
		if n > max {
			max = n
		}
		collisions[n]++
	}
	// Turn this data into a slice
	freq := make([]int64, max)
	for i, n := range collisions {
		freq[i-1] = n
	}
	// Output the table
	t := tablewriter.NewWriter(os.Stdout)
	t.SetBorder(false)
	t.SetHeader([]string{"#", "frequency"})
	t.SetColumnAlignment([]int{tablewriter.ALIGN_RIGHT, tablewriter.ALIGN_LEFT})
	if max <= 10 {
		for i := 0; i < max; i++ {
			t.Append([]string{
				strconv.Itoa(i + 1),
				strconv.FormatInt(freq[i], 10),
			})
		}
	} else {
		for i := 0; i < 10; i++ {
			t.Append([]string{
				strconv.Itoa(i + 1),
				strconv.FormatInt(freq[i], 10),
			})
		}
		t.Append([]string{"...", "..."})
		t.Append([]string{
			strconv.Itoa(max),
			strconv.FormatInt(freq[max-1], 10),
		})
	}
	t.Render()
}

// outputInverseMap writes the inverse hash map m to the writer w.
func outputInverseMap(w io.Writer, m map[hash.Value][]int64) error {
	// Sort the hashes
	hs := make([]hash.Value, 0, len(m))
	for h := range m {
		hs = append(hs, h)
	}
	sort.Slice(hs, func(i int, j int) bool {
		return hs[i] < hs[j]
	})
	// Create an encoding buffer
	buf := make([]byte, binary.MaxVarintLen64)
	// Output the hash values and corresponding IDs
	for _, h := range hs {
		// Output the hash value
		n := binary.PutVarint(buf, int64(h))
		_, err := w.Write(buf[:n])
		if err != nil {
			return err
		}
		// Output the IDs
		size := len(m[h])
		for i, id := range m[h] {
			// We use a negative ID to indicate the final entry in the slice
			if i == size-1 {
				id *= -1
			}
			// Output the ID
			n = binary.PutVarint(buf, id)
			_, err = w.Write(buf[:n])
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// runMain executes the main function, returning any errors.
func runMain(opts *Options) error {
	// Open the database file
	r, err := os.Open(opts.DB)
	if err != nil {
		return err
	}
	defer r.Close()
	// Load the DB
	db, err := ks4.Read(r)
	if err != nil {
		return err
	}
	// Create the output file
	w, err := os.Create(opts.Output)
	if err != nil {
		return err
	}
	defer w.Close()
	// Wrap the output file in a buffer
	bw := bufio.NewWriter(w)
	defer bw.Flush()
	// Build the inverse hash map -- we do this in stages because it's too large
	// to build in one go
	var min, max uint64
	max = hash.Max / 10
	for {
		// Are we done?
		if min > hash.Max {
			return nil
		}
		// Build the inverse map
		m, err := buildInverseMap(db, hash.Value(min), hash.Value(max))
		if err != nil {
			return err
		}
		// Output the statistics about the map
		printInverseMapStats(m)
		// Write out the inverse hash map
		err = outputInverseMap(bw, m)
		if err != nil {
			return err
		}
		// Move on
		min = max + 1
		max += hash.Max / 10
		if max > hash.Max {
			max = hash.Max
		}
	}
}

// main is the program entry point.
func main() {
	assertNoErr(runMain(setOptions()))
}
