/*
ks4makeindex creates the index file used to seek to the correct location in the binary data.

This is STEP 3 of 7.
*/

package main

import (
	"bitbucket.org/pcas/ks4binary/ks4"
	"bitbucket.org/pcas/ks4binary/point"
	"bitbucket.org/pcastools/flag"
	"bufio"
	"errors"
	"fmt"
	"github.com/gosuri/uiprogress"
	"io"
	"os"
	"runtime"
)

// Options describes the options.
type Options struct {
	Raw   string
	Dict  string
	Index string
}

// The default values.
const (
	DefaultRaw   = "ks4.raw"
	DefaultDict  = "ks4.dict"
	DefaultIndex = "ks4.idx"
)

// stepSize is the step size used when building index data.
const stepSize = 5000

// progressBarInterval defines how frequently the progress bar is updated.
const progressBarInterval = 100000

// Name is the name of the executable.
const Name = "ks4makeindex"

// positionReader is an io.ByteReader that knows its current read offset.
type positionReader struct {
	offset int64         // The read offset
	r      io.ByteReader // The underlying byte reader
}

// Offset returns the current read offset.
func (r *positionReader) Offset() int64 {
	return r.offset
}

// ReadByte reads and returns the next byte from the reader.
func (r *positionReader) ReadByte() (byte, error) {
	b, err := r.r.ReadByte()
	if err == nil {
		r.offset++
	}
	return b, err
}

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := &Options{
		Raw:   DefaultRaw,
		Dict:  DefaultDict,
		Index: DefaultIndex,
	}
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s generates an index for the 4-dimensional Kreuzer-Skarke database.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("raw", &opts.Raw, opts.Raw, "The raw data", ""),
		flag.String("dict", &opts.Dict, opts.Dict, "The dictionary", ""),
		flag.String("index", &opts.Index, opts.Index, "The new index", ""),
	)
	// Parse the flags
	flag.Parse()
	// Check that the paths are non-empty
	if len(opts.Raw) == 0 {
		return errors.New("no raw data path is specified")
	} else if len(opts.Index) == 0 {
		return errors.New("no destination index path is specified")
	}
	// Looks good
	return nil
}

// createIndex generates the index.
func createIndex(dbpath string, d *point.Dict) (*point.Index, error) {
	// Open the database file and wrap it in a position reader
	r, err := os.Open(dbpath)
	if err != nil {
		return nil, err
	}
	defer r.Close()
	br := &positionReader{
		r: bufio.NewReader(r),
	}
	// Start the progress bar drawer running in its own go-routine
	prog := uiprogress.New()
	go prog.Start()
	// Create the progress bar
	bar := prog.AddBar(ks4.DBSize / progressBarInterval)
	bar.PrependCompleted()
	// Build the slice of positions
	position := make([]int64, 0, ks4.DBSize/stepSize+1)
	position = append(position, 0)
	err = ks4.ForEach(br, d, func(id int64, _ point.Points) error {
		if id%stepSize == 0 {
			position = append(position, br.Offset())
		}
		if id%progressBarInterval == 0 {
			bar.Incr()
		}
		return nil
	})
	// Stop the progress bar and handle any errors
	prog.Stop()
	if err != nil {
		return nil, err
	}
	// Return the index
	return point.NewIndex(position, stepSize)
}

// loadDict loads the dictionary from the given path.
func loadDict(path string) (*point.Dict, error) {
	// Is there anything to do?
	if len(path) == 0 {
		return nil, nil
	}
	// Open the file
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	// Read in the dictionary
	return point.ReadDict(bufio.NewReader(f))
}

// runMain executes the main function, returning any errors.
func runMain(opts *Options) error {
	// Read the dictionary
	d, err := loadDict(opts.Dict)
	if err != nil {
		return err
	}
	// Generate the index data
	idx, err := createIndex(opts.Raw, d)
	if err != nil {
		return err
	}
	// Open the output file for writing. Any existing file will be replaced.
	w, err := os.Create(opts.Index)
	if err != nil {
		return err
	}
	defer w.Close()
	// Wrap the output file in a buffer
	bw := bufio.NewWriter(w)
	defer bw.Flush()
	// Output the index
	_, err = idx.WriteTo(bw)
	return err
}

// main is the program entry point.
func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	assertNoErr(runMain(setOptions()))
}
