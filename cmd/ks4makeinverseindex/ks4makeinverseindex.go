/*
ks4makeinverseindex creates the index file used to seek to the correct location in the inverse data.

This is STEP 6 of 7.
*/

package main

import (
	"bitbucket.org/pcas/ks4binary/hashlookup"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/hash"
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"runtime"
)

// Options describes the options.
type Options struct {
	Raw   string
	Dict  string
	Index string
}

// The default values.
const (
	DefaultRaw   = "ks4inv.raw"
	DefaultIndex = "ks4inv.idx"
)

// stepSize is the step size used when building index data.
const stepSize = 1000

// progressBarInterval defines how frequently the progress bar is updated.
const progressBarInterval = 100000

// Name is the name of the executable.
const Name = "ks4makeinverseindex"

// positionReader is an io.ByteReader that knows its current read offset.
type positionReader struct {
	offset int64         // The read offset
	r      io.ByteReader // The underlying byte reader
}

// Offset returns the current read offset.
func (r *positionReader) Offset() int64 {
	return r.offset
}

// ReadByte reads and returns the next byte from the reader.
func (r *positionReader) ReadByte() (byte, error) {
	b, err := r.r.ReadByte()
	if err == nil {
		r.offset++
	}
	return b, err
}

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := &Options{
		Raw:   DefaultRaw,
		Index: DefaultIndex,
	}
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s generates an index for the 4-dimensional Kreuzer-Skarke inverse database.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("raw", &opts.Raw, opts.Raw, "The raw inverse data", ""),
		flag.String("index", &opts.Index, opts.Index, "The new index", ""),
	)
	// Parse the flags
	flag.Parse()
	// Check that the paths are non-empty
	if len(opts.Raw) == 0 {
		return errors.New("no raw inverse data path is specified")
	} else if len(opts.Index) == 0 {
		return errors.New("no destination index path is specified")
	}
	// Looks good
	return nil
}

// createIndex generates the index.
func createIndex(dbpath string) (*hashlookup.Index, error) {
	// Open the database file and wrap it in a position reader
	r, err := os.Open(dbpath)
	if err != nil {
		return nil, err
	}
	defer r.Close()
	br := &positionReader{
		r: bufio.NewReader(r),
	}
	// Build the slice of positions
	position := make([]int64, 0)
	position = append(position, 0)
	var lastRecord hash.Value
	h, _, err := hashlookup.Decode(br)
	for err == nil {
		// Do we need to record the position?
		i := h / stepSize
		for i > lastRecord {
			position = append(position, br.Offset())
			lastRecord++
		}
		// Move on
		h, _, err = hashlookup.Decode(br)
	}
	// Handle any errors
	if err != io.EOF {
		return nil, err
	}
	return hashlookup.NewIndex(position, stepSize)
}

// runMain executes the main function, returning any errors.
func runMain(opts *Options) error {
	// Generate the index data
	idx, err := createIndex(opts.Raw)
	if err != nil {
		return err
	}
	// Open the output file for writing. Any existing file will be replaced.
	w, err := os.Create(opts.Index)
	if err != nil {
		return err
	}
	defer w.Close()
	// Wrap the output file in a buffer
	bw := bufio.NewWriter(w)
	defer bw.Flush()
	// Output the index
	_, err = idx.WriteTo(bw)
	return err
}

// main is the program entry point.
func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	assertNoErr(runMain(setOptions()))
}
