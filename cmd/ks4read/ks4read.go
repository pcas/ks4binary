/*
ks4read will output the points of given ID in the K-S database.
*/

package main

import (
	"bitbucket.org/pcas/ks4binary/ks4"
	"bitbucket.org/pcastools/flag"
	"errors"
	"fmt"
	"os"
	"strconv"
)

// Options describes the options.
type Options struct {
	DB  string
	IDs []int64
}

// The default values.
const (
	DefaultDB = "ks4.db"
)

// Name is the name of the executable.
const Name = "ks4read"

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := &Options{
		DB: DefaultDB,
	}
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s reads points from the 4-dimensional Kreuzer-Skarke database.\n\nUsage: %s [options] id1 [... idN]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("db", &opts.DB, opts.DB, "The database", ""),
	)
	// Parse the flags
	flag.Parse()
	// Extract the ids
	ids := make([]int64, 0, flag.NArg())
	for i, s := range flag.Args() {
		id, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			return fmt.Errorf("unable to parse ID %d: %w", i+1, err)
		} else if id <= 0 || id > ks4.DBSize {
			return fmt.Errorf("ID %d (%d) should be in the range 1..%d", i+1, id, ks4.DBSize)
		}
		ids = append(ids, id)
	}
	opts.IDs = ids
	// Check that the database to use is non-empty
	if len(opts.DB) == 0 {
		return errors.New("no database path is specified")
	}
	// Looks good
	return nil
}

// printIDs prints the points for the given IDs from the database.
func printIDs(db *ks4.DB, ids []int64) error {
	for _, id := range ids {
		pts, err := db.Get(id)
		if err != nil {
			return err
		}
		fmt.Printf("%d:%s\n", id, pts)
	}
	return nil
}

// runMain executes the main function, returning any errors.
func runMain(opts *Options) error {
	// Open the database file
	r, err := os.Open(opts.DB)
	if err != nil {
		return err
	}
	defer r.Close()
	// Load the DB
	db, err := ks4.Read(r)
	if err != nil {
		return err
	}
	// Output the points
	return printIDs(db, opts.IDs)
}

// main is the program entry point.
func main() {
	assertNoErr(runMain(setOptions()))
}
