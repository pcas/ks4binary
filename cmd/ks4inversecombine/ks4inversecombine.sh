#!/bin/bash

###
# ks4inversecombine combines the index file and raw binary data into
# a single file for performing inverse lookups.
#
# This is STEP 7 of 7.
###

# Sanity check
if [ ! -e "ks4inv.idx" ]; then
    echo "unable to locate ks4.idx"
    exit 1
elif [ ! -e "ks4inv.raw" ]; then
    echo "unable to locate ks4inv.raw"
    exit 1
elif [ -e "ks4inv.db" ]; then
    echo "output file ks4inv.db already exists"
    exit 1
fi

# Combine the files
cat ks4inv.idx > ks4inv.db
cat ks4inv.raw >> ks4inv.db