// hashlookup provides an inverse lookup of hashes of points to IDs.

package hashlookup

import (
	"bitbucket.org/pcastools/hash"
	"encoding/binary"
	"fmt"
	"io"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Decode reads in the next hash value and matching IDs from r.
func Decode(r io.ByteReader) (hash.Value, []int64, error) {
	// The first value is the hash value
	n, err := binary.ReadVarint(r)
	if err != nil {
		if err != io.EOF {
			err = fmt.Errorf("error reading hash value: %w", err)
		}
		return 0, nil, err
	} else if n < 0 || n > hash.Max {
		return 0, nil, fmt.Errorf("invalid hash value: %d", n)
	}
	h := hash.Value(n)
	// Read in the slice of IDs
	var done bool
	ids := make([]int64, 0)
	for !done {
		// Read in the next ID
		n, err = binary.ReadVarint(r)
		if err != nil {
			return 0, nil, fmt.Errorf("error reading ID for hash value %d: %w", h, err)
		} else if n == 0 {
			return 0, nil, fmt.Errorf("invalid ID for has value %d", h)
		}
		// Fix the sign if necessary -- a negative ID means that this is the
		// final entry in the slice.
		if n < 0 {
			done = true
			n *= -1
		}
		// Add the ID to the slice
		ids = append(ids, n)
	}
	// Return the data
	return h, ids, nil
}
