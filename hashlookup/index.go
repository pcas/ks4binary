// Index provides an index for an inverse lookup of hashes of points to IDs.

package hashlookup

import (
	"bitbucket.org/pcastools/hash"
	"encoding/binary"
	"fmt"
	"io"
)

// Index is an index of seek positions in the database.
type Index struct {
	stepSize hash.Value // The index step size
	position []int64    // The seek positions that form the index
}

/////////////////////////////////////////////////////////////////////////
// Index functions
/////////////////////////////////////////////////////////////////////////

// Position returns the largest offset position less than or equal to the given hash.
func (idx *Index) Position(h hash.Value) int64 {
	i := int(h / idx.stepSize)
	if i >= len(idx.position) {
		i = len(idx.position) - 1
	}
	return idx.position[i]
}

// WriteTo writes the index to w. Returns the number of bytes written.
func (idx *Index) WriteTo(w io.Writer) (int64, error) {
	// Create a buffer
	buf := make([]byte, binary.MaxVarintLen64)
	// Keep track of the total number of bytes written
	var n int64
	if idx != nil {
		// First we output the step size
		m := binary.PutVarint(buf, int64(idx.stepSize))
		m, err := w.Write(buf[:m])
		n += int64(m)
		if err != nil {
			return n, fmt.Errorf("error writing index step size: %w", err)
		}
		// Output the positions
		var last int64
		for i, pos := range idx.position {
			m = binary.PutVarint(buf, pos-last)
			m, err := w.Write(buf[:m])
			n += int64(m)
			if err != nil {
				return n, fmt.Errorf("error writing position at index %d: %w", i, err)
			}
			last = pos
		}
	}
	// Write out -1, indicating the end of the index
	m := binary.PutVarint(buf, -1)
	m, err := w.Write(buf[:m])
	n += int64(m)
	if err != nil {
		return n, fmt.Errorf("error writing index: %w", err)
	}
	return n, nil
}

// ReadIndex reads an index from r.
func ReadIndex(r io.ByteReader) (*Index, error) {
	// Create an index
	idx := &Index{}
	// The first value is the stepsize (or -1 if the index is empty)
	m, err := binary.ReadVarint(r)
	if err != nil {
		return nil, fmt.Errorf("error reading index step size: %w", err)
	} else if m <= 0 || m > hash.Max {
		if m == -1 {
			return nil, nil
		}
		return nil, fmt.Errorf("invalid step size (%d)", m)
	}
	idx.stepSize = hash.Value(m)
	// Read in the positions (-1 indicates the end of the index)
	last := int64(0)
	m, err = binary.ReadVarint(r)
	for err == nil && m != -1 {
		// Sanity check
		if m < 0 {
			return nil, fmt.Errorf("invalid position at index %d", len(idx.position))
		}
		// Add the position
		pos := m + last
		idx.position = append(idx.position, pos)
		// Move on
		last = pos
		m, err = binary.ReadVarint(r)
	}
	// Handle any errors
	if err != nil {
		return nil, fmt.Errorf("error reading index position at index %d: %w", len(idx.position), err)
	}
	return idx, nil
}

// NewIndex returns a new index described by the given positions and step size.
func NewIndex(position []int64, stepSize hash.Value) (*Index, error) {
	// Sanity check
	lastn := int64(-1)
	for i, n := range position {
		if n < lastn {
			return nil, fmt.Errorf("invalid position at index %d", i)
		}
		lastn = n
	}
	// Make a copy of the positions
	pos := make([]int64, len(position))
	copy(pos, position)
	// Return the index
	return &Index{
		stepSize: stepSize,
		position: pos,
	}, nil
}
