// ks4 provides a functions for working with the ks4 database.

package ks4

import (
	"bitbucket.org/pcas/ks4binary/point"
	"bufio"
	"fmt"
	"io"
	"sync"
)

// DBSize is the number of entries in the Kreuzer-Skarke database of 4-dimensional reflexive polytopes.
const DBSize = 473800776

// ReadSeeker is an io.Reader with Seek.
type ReadSeeker interface {
	io.Reader
	// Seek sets the offset for the next Read to offset, interpreted
	// according to whence: 0 means relative to the origin, 1 means
	// relative to the current offset, and 2 means relative to the end.
	// It returns the new offset and an error, if any.
	Seek(offset int64, whence int) (int64, error)
}

// positionReader is an io.ByteReader that knows its current read offset.
type positionReader struct {
	io.ByteReader
	offset int64 // The read offset
}

// offsetReader is a ReadSeeker that offsets all Seek's relative to the origin by a fixed amount.
type offsetReader struct {
	ReadSeeker
	offset int64 // The seek offset
}

// DB is a database of points. A DB is safe for concurrent use.
type DB struct {
	d      *point.Dict   // The decode dictionary
	idx    *point.Index  // The position index
	m      sync.Mutex    // Mutex protecting the following
	nextid int64         // The next ID
	r      ReadSeeker    // The underlying database
	b      *bufio.Reader // The current read buffer
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// execUserFunc executes the user-provided function f with the given arguments, recovering from any panic.
func execUserFunc(f func(int64, point.Points) error, id int64, pts point.Points) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in user-provided function: %v", e)
		}
	}()
	err = f(id, pts)
	return
}

/////////////////////////////////////////////////////////////////////////
// positionReader functions
/////////////////////////////////////////////////////////////////////////

// Offset returns the current read offset.
func (r *positionReader) Offset() int64 {
	return r.offset
}

// ReadByte reads and returns the next byte from the reader.
func (r *positionReader) ReadByte() (byte, error) {
	b, err := r.ByteReader.ReadByte()
	if err == nil {
		r.offset++
	}
	return b, err
}

/////////////////////////////////////////////////////////////////////////
// offsetReader functions
/////////////////////////////////////////////////////////////////////////

// Seek sets the offset for the next Read to offset, interpreted according to whence: 0 means relative to the origin, 1 means relative to the current offset, and 2 means relative to the end. It returns the new offset and an error, if any.
func (r *offsetReader) Seek(offset int64, whence int) (int64, error) {
	if whence == 0 {
		offset += r.offset
	}
	return r.ReadSeeker.Seek(offset, whence)
}

/////////////////////////////////////////////////////////////////////////
// DB functions
/////////////////////////////////////////////////////////////////////////

// Read reads a DB from r.
func Read(r ReadSeeker) (*DB, error) {
	// Wrap r as a position reader
	pr := &positionReader{
		ByteReader: bufio.NewReader(r),
	}
	// Load the dictionary
	d, err := point.ReadDict(pr)
	if err != nil {
		return nil, err
	}
	// Load the index
	idx, err := point.ReadIndex(pr)
	if err != nil {
		return nil, err
	}
	// Note the current offset -- this is the start of the binary data
	offset := pr.Offset()
	// Reset r and return the DB
	_, err = r.Seek(offset, 0)
	if err != nil {
		return nil, fmt.Errorf("unable to seek to start position: %w", err)
	}
	return New(&offsetReader{
		ReadSeeker: r,
		offset:     offset,
	}, d, idx), nil
}

// New returns a new DB using the given dictionary d (which may be nil), index idx (which may be nil), and database r.
func New(r ReadSeeker, d *point.Dict, idx *point.Index) *DB {
	return &DB{
		nextid: 1,
		d:      d,
		idx:    idx,
		r:      r,
		b:      bufio.NewReader(r),
	}
}

// Get returns the points with given ID from the database.
//
// Note that database IDs are indexed from 1.
func (db *DB) Get(id int64) (point.Points, error) {
	// Sanity check
	if id <= 0 || id > DBSize {
		return nil, fmt.Errorf("invalid database ID (%d)", id)
	}
	// Acquire a lock
	db.m.Lock()
	defer db.m.Unlock()
	// Move to the correct location in the database
	if id != db.nextid {
		offset, skip := db.idx.Position(id)
		if id < db.nextid || skip < id-db.nextid {
			// Seek to the new offset and reset the buffer
			_, err := db.r.Seek(offset, 0)
			if err != nil {
				return nil, fmt.Errorf("unable to seek to new position: %w", err)
			}
			db.b.Reset(db.r)
			// Update the index
			db.nextid = id - skip
		} else {
			skip = id - db.nextid
		}
		// Skip over the required number of entries
		for skip > 0 {
			_, err := db.d.DecodePoints(db.b)
			if err != nil {
				return nil, fmt.Errorf("error reading entry %d: %w", db.nextid, err)
			}
			skip--
			db.nextid++
		}
	}
	// Read in the next entry
	pts, err := db.d.DecodePoints(db.b)
	if err != nil {
		return nil, fmt.Errorf("error reading entry %d: %w", db.nextid, err)
	}
	db.nextid++
	return pts, err
}

// ForEach iterators over the database, calling the function f for each entry.
//
// Note that database IDs are indexed from 1.
func (db *DB) ForEach(f func(int64, point.Points) error) error {
	// Start iterating over the entries
	for id := int64(1); id <= DBSize; id++ {
		// Read in the next entry
		pts, err := db.Get(id)
		if err != nil {
			return err
		}
		// Call the user function
		err = execUserFunc(f, id, pts)
		if err != nil {
			return err
		}
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ForEach iterates over the database r using the given dictionary d (which may be nil), calling the function f for each entry.
//
// Note that database IDs are indexed from 1.
func ForEach(r io.ByteReader, d *point.Dict, f func(int64, point.Points) error) error {
	// Start iterating over the entries
	id := int64(1)
	for {
		// Read in the next entry
		pts, err := d.DecodePoints(r)
		if err != nil {
			if err == io.EOF {
				return nil
			}
			return fmt.Errorf("error reading entry %d: %w", id, err)
		}
		// Call the user function
		err = execUserFunc(f, id, pts)
		if err != nil {
			return err
		}
		// Move on
		id++
	}
}
