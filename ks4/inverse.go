// inverse provides an inverse lookup of points to ID.

package ks4

import (
	"bitbucket.org/pcas/ks4binary/hashlookup"
	"bitbucket.org/pcas/ks4binary/point"
	"bitbucket.org/pcastools/hash"
	"bufio"
	"fmt"
	"io"
	"sync"
)

// InverseDB defines a database of points to ID.
type InverseDB struct {
	db  *DB               // The database of points
	idx *hashlookup.Index // The position index
	m   sync.Mutex        // Mutex protecting the following
	r   ReadSeeker        // The underlying database
	b   *bufio.Reader     // The current read buffer
}

/////////////////////////////////////////////////////////////////////////
// DB functions
/////////////////////////////////////////////////////////////////////////

// GetForHash returns the IDs for the given hash value.
//
// Note that database IDs are indexed from 1.
func (db *InverseDB) GetForHash(h hash.Value) ([]int64, error) {
	// Compute the seek position
	offset := db.idx.Position(h)
	// Acquire a lock
	db.m.Lock()
	defer db.m.Unlock()
	// Seek to the closest offset and reset the buffer
	_, err := db.r.Seek(offset, 0)
	if err != nil {
		return nil, fmt.Errorf("unable to seek to new position: %w", err)
	}
	db.b.Reset(db.r)
	// Start reading entries until we find the hash value we're looking for
	for {
		curh, ids, err := hashlookup.Decode(db.b)
		if err != nil {
			if err == io.EOF {
				return nil, nil
			}
			return nil, fmt.Errorf("error reading entry: %w", err)
		} else if curh == h {
			return ids, nil
		} else if curh > h {
			return nil, nil
		}
	}
}

// Get returns the ID for the given points, or 0 if the points are not in the database.
//
// Note that database IDs are indexed from 1.
func (db *InverseDB) Get(pts point.Points) (int64, error) {
	// Fetch the possible IDs
	ids, err := db.GetForHash(pts.Hash())
	if err != nil {
		return 0, err
	} else if len(ids) == 0 {
		return 0, nil
	}
	// Start working through the possible IDs looking for a match
	for _, id := range ids {
		ppts, err := db.db.Get(id)
		if err != nil {
			return 0, err
		} else if pts.Equals(ppts) {
			return id, nil
		}
	}
	return 0, nil
}

// ReadInverseDB reads an inverse DB from r, using database of points db.
func ReadInverseDB(r ReadSeeker, db *DB) (*InverseDB, error) {
	// Wrap r as a position reader
	pr := &positionReader{
		ByteReader: bufio.NewReader(r),
	}
	// Load the index
	idx, err := hashlookup.ReadIndex(pr)
	if err != nil {
		return nil, err
	}
	// Note the current offset -- this is the start of the binary data
	offset := pr.Offset()
	// Reset r and return the DB
	_, err = r.Seek(offset, 0)
	if err != nil {
		return nil, fmt.Errorf("unable to seek to start position: %w", err)
	}
	return NewInverseDB(&offsetReader{
		ReadSeeker: r,
		offset:     offset,
	}, idx, db), nil
}

// NewInverseDB returns a new inverse DB using the given index idx (which may be nil), inverse database r, and database of points db.
func NewInverseDB(r ReadSeeker, idx *hashlookup.Index, db *DB) *InverseDB {
	return &InverseDB{
		db:  db,
		idx: idx,
		r:   r,
		b:   bufio.NewReader(r),
	}
}
