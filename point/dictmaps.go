// Dictmaps implements dictionaries used by a Dict.

package point

// minDictID is the smallest ID in a dictionary. This is the smallest positive integer that does not appear as the first coordinate of a vertex in the Kreuzer-Skarke database.
const minDictID = 148

// decodeMap is a map from an ID to a slice of points.
type decodeMap struct {
	dicts [][]Points
}

// encodeMap is a map from a slice of points to an ID.
type encodeMap struct {
	dict Map
}

/////////////////////////////////////////////////////////////////////////
// decodeMap functions
/////////////////////////////////////////////////////////////////////////

// Len returns the size of the map.
func (S *decodeMap) Len() (n int) {
	if S != nil {
		for _, d := range S.dicts {
			n += len(d)
		}
	}
	return n
}

// Entry returns the points for the given ID, if known. The second return value is true iff the ID exists in the map.
func (S *decodeMap) Entry(i int64) (Points, bool) {
	if S != nil && i >= 0 {
		idx := int(i - minDictID)
		for _, d := range S.dicts {
			size := len(d)
			if idx < size {
				return d[idx], true
			}
			idx -= size
		}
	}
	return nil, false
}

// Scan will scan the points with given ID. The second return value is true iff the ID exists in the map. If true, also returns the number of points scanned, or -1 if the length was too short to hold the points.
func (S *decodeMap) Scan(i int64, dst Points) (int, bool) {
	if S == nil || i < 0 {
		return 0, false
	}
	idx := int(i - minDictID)
	for _, d := range S.dicts {
		size := len(d)
		if idx < size {
			src := d[idx]
			n := len(src)
			if len(dst) < n {
				return -1, true
			}
			for j := 0; j < n; j++ {
				dst[j] = src[j]
			}
			return n, true
		}
		idx -= size
	}
	return 0, false
}

// PointsOfLen returns a slice of all points in the map of given length.
func (S *decodeMap) PointsOfLen(k int) []Points {
	if S == nil || k < 1 || k > len(S.dicts) {
		return nil
	}
	return S.dicts[k-1]
}

// Set replaces (or sets) the dictionary entries for slices of points of length n to pts and returns a new map. Note that no checks are made that these points are actually of length n, nor that the value of n is sensible. Note also that, after adding a slice of points, any previously encoded data will be undecodable with this map.
func (S *decodeMap) Set(n int, pts []Points) *decodeMap {
	// Is there anything to do?
	if len(pts) == 0 {
		return S
	}
	// Make a copy of S
	T := &decodeMap{}
	if S != nil {
		for i, d := range S.dicts {
			if i == n-1 || len(d) == 0 {
				T.dicts = append(T.dicts, nil)
			} else {
				newd := make([]Points, len(d))
				copy(newd, d)
				T.dicts = append(T.dicts, newd)
			}
		}
	}
	// Set the points
	if len(T.dicts) < n {
		for i := len(T.dicts); i <= n; i++ {
			T.dicts = append(T.dicts, nil)
		}
	}
	T.dicts[n-1] = pts
	return T
}

// Append adds the slice of points to the appropriate place in the map and returns the new map. Note that no checks are made that these points are not already in the map. Note also that, after adding a slice of points, any previously encoded data will be undecodable with this map.
func (S *decodeMap) Append(pts ...Points) *decodeMap {
	// Is there anything to do?
	if len(pts) == 0 {
		return S
	}
	// Make a copy of S
	T := &decodeMap{}
	if S != nil {
		for _, d := range S.dicts {
			if len(d) == 0 {
				T.dicts = append(T.dicts, nil)
			} else {
				newd := make([]Points, len(d))
				copy(newd, d)
				T.dicts = append(T.dicts, newd)
			}
		}
	}
	// Add the points in blocks of equal length
	for len(pts) != 0 {
		n := len(pts[0])
		if n == 0 {
			panic("points must have non-zero length")
		}
		finish := len(pts)
		for i := range pts {
			if len(pts[i]) != n {
				finish = i
				break
			}
		}
		if len(T.dicts) < n {
			for i := len(T.dicts); i <= n; i++ {
				T.dicts = append(T.dicts, nil)
			}
		}
		T.dicts[n-1] = append(T.dicts[n-1], pts[:finish]...)
		pts = pts[finish:]
	}
	return T
}

/////////////////////////////////////////////////////////////////////////
// encoder functions
/////////////////////////////////////////////////////////////////////////

// newEncodeMap returns a new encode map for the given decode map.
func newEncodeMap(S *decodeMap) *encodeMap {
	// Create a new encoder
	T := &encodeMap{}
	// Create the map
	n := S.Len()
	for i := 0; i < n; i++ {
		if pts, ok := S.Entry(int64(i) + minDictID); ok {
			T.dict.Set(pts, i)
		}
	}
	return T
}

// Len returns the size of the map.
func (S *encodeMap) Len() (n int) {
	if S != nil {
		n = S.dict.Len()
	}
	return
}

// Entry returns the ID for the given points, if known. The second return value is true iff the points exists in the map.
func (S *encodeMap) Entry(pts Points) (id int64, ok bool) {
	if S != nil {
		var x interface{}
		if x, ok = S.dict.Get(pts); ok {
			id = int64(x.(int)) + minDictID
		}
	}
	return
}
