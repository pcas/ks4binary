// Index provides an index for a database.

package point

import (
	"encoding/binary"
	"fmt"
	"io"
)

// Index is an index of seek positions in the database.
type Index struct {
	stepSize int64   // The index step size
	position []int64 // The seek positions that form the index
}

/////////////////////////////////////////////////////////////////////////
// Index functions
/////////////////////////////////////////////////////////////////////////

// Position returns the position for the given ID, followed by the number of additional entries which need to be skipped to reach ID.
//
// Note that database IDs are indexed from 1.
func (idx *Index) Position(id int64) (int64, int64) {
	// Sanity check
	if id <= 0 {
		return 0, 0
	} else if idx == nil || idx.stepSize == 0 {
		return 0, id - 1
	}
	// Consult the index
	i := int((id - 1) / idx.stepSize)
	if i >= len(idx.position) {
		i = len(idx.position) - 1
	}
	return idx.position[i], id - 1 - int64(i)*idx.stepSize
}

// WriteTo writes the index to w. Returns the number of bytes written.
func (idx *Index) WriteTo(w io.Writer) (int64, error) {
	// Fetch a buffer
	buf := bufPool.Get().([]byte)
	defer bufPool.Put(buf)
	// Keep track of the total number of bytes written
	var n int64
	if idx != nil {
		// First we output the step size
		m := binary.PutVarint(buf, idx.stepSize)
		m, err := w.Write(buf[:m])
		n += int64(m)
		if err != nil {
			return n, fmt.Errorf("error writing index step size: %w", err)
		}
		// Output the positions
		var last int64
		for i, pos := range idx.position {
			m = binary.PutVarint(buf, pos-last)
			m, err := w.Write(buf[:m])
			n += int64(m)
			if err != nil {
				return n, fmt.Errorf("error writing position at index %d: %w", i, err)
			}
			last = pos
		}
	}
	// Write out -1, indicating the end of the index
	m := binary.PutVarint(buf, -1)
	m, err := w.Write(buf[:m])
	n += int64(m)
	if err != nil {
		return n, fmt.Errorf("error writing index: %w", err)
	}
	return n, nil
}

// ReadIndex reads an index from r.
func ReadIndex(r io.ByteReader) (*Index, error) {
	// Create an index
	idx := &Index{}
	// The first value is the stepsize (or -1 if the index is empty)
	m, err := binary.ReadVarint(r)
	if err != nil {
		return nil, fmt.Errorf("error reading index step size: %w", err)
	} else if m <= 0 {
		if m == -1 {
			return nil, nil
		}
		return nil, fmt.Errorf("the step size (%d) must be a positive integer", m)
	}
	idx.stepSize = m
	// Read in the positions (-1 indicates the end of the index)
	last := int64(0)
	m, err = binary.ReadVarint(r)
	for err == nil && m != -1 {
		// Sanity check
		if m < 0 {
			return nil, fmt.Errorf("invalid position at index %d", len(idx.position))
		}
		// Add the position
		pos := m + last
		idx.position = append(idx.position, pos)
		// Move on
		last = pos
		m, err = binary.ReadVarint(r)
	}
	// Handle any errors
	if err != nil {
		return nil, fmt.Errorf("error reading index position at index %d: %w", len(idx.position), err)
	}
	return idx, nil
}

// NewIndex returns a new index described by the given positions and step size.
func NewIndex(position []int64, stepSize int64) (*Index, error) {
	// Sanity check
	if stepSize <= 0 {
		return nil, fmt.Errorf("the step size (%d) must be a positive integer", stepSize)
	}
	lastn := int64(-1)
	for i, n := range position {
		if n <= lastn {
			return nil, fmt.Errorf("invalid position at index %d", i)
		}
		lastn = n
	}
	// Make a copy of the positions
	pos := make([]int64, len(position))
	copy(pos, position)
	// Return the index
	return &Index{
		stepSize: stepSize,
		position: pos,
	}, nil
}
