package point

import (
	"math/rand"
	"sync"
	"sync/atomic"
)

// Counter is a counter the number of points. This is safe for concurrent use.
type Counter struct {
	n       int64         // The number of elements in the counter
	maxSize int           // The maximum number of elements in the counter
	m       []*sync.Mutex // Protects the following
	counts  []*Map        // The map of points to values
}

/////////////////////////////////////////////////////////////////////////
// Counter functions
/////////////////////////////////////////////////////////////////////////

// NewCounter returns a new initialised counter ready for use. The counter will use the indicated number of buckets (more buckets decreases lock contention), and will grow to the given size before pruning. A value of maxSize <= 0 indicates that no pruning will take place.
func NewCounter(numBuckets int, maxSize int) *Counter {
	// Sanity check
	if numBuckets <= 0 {
		numBuckets = 1
	}
	// Create the counter
	c := &Counter{
		maxSize: maxSize,
		m:       make([]*sync.Mutex, 0, numBuckets),
		counts:  make([]*Map, 0, numBuckets),
	}
	for i := 0; i < numBuckets; i++ {
		c.m = append(c.m, &sync.Mutex{})
		c.counts = append(c.counts, &Map{})
	}
	return c
}

// Len returns the number of points in the counter.
func (S *Counter) Len() int {
	if S == nil {
		return 0
	}
	return int(atomic.LoadInt64(&S.n))
}

// prune deletes points from the i-th bucket, starting with the smallest value and working up, until TODO. Assumes that the caller holds a lock on the i-th bucket.
func (S *Counter) prune(i int) {
	// Is there anything to do?
	if S == nil || S.maxSize <= 0 || int(atomic.LoadInt64(&S.n)) <= S.maxSize {
		return
	}
	// Start pruning points with count value k
	var num int
	counts := S.counts[i]
	size := counts.Len()
	for k := 1; size > 0; k++ {
		// Extract the points with count value k
		T := make([]Points, 0)
		counts.ForEach(func(pts Points, x interface{}) bool {
			if x.(int) == k {
				T = append(T, pts)
			}
			return true
		})
		// Shuffle the points (if necessary)
		if num+len(T) > 100 {
			rand.Shuffle(len(T), func(i int, j int) {
				T[i], T[j] = T[j], T[i]
			})
		}
		// Delete the points
		for i, pts := range T {
			counts.Delete(pts)
			num++
			// Are we done?
			if num == 100 {
				atomic.AddInt64(&S.n, -int64(i+1))
				return
			}
		}
		// Move on
		atomic.AddInt64(&S.n, -int64(len(T)))
		size -= len(T)
	}
}

// Increment increments the value for the given points. Returns the new value.
func (S *Counter) Increment(pts Points) int {
	var n int
	i := int(pts.Hash()) % len(S.m)
	m, counts := S.m[i], S.counts[i]
	m.Lock()
	if x, ok := counts.Get(pts); ok {
		n = x.(int)
	} else {
		S.prune(i)
		atomic.AddInt64(&S.n, 1)
	}
	counts.Set(pts, n+1)
	m.Unlock()
	return n + 1
}

// Get returns the value for the given points (which will be 0 if the points were not in the counter).
func (S *Counter) Get(pts Points) int {
	if S == nil {
		return 0
	}
	var n int
	i := int(pts.Hash()) % len(S.m)
	m := S.m[i]
	m.Lock()
	if x, ok := S.counts[i].Get(pts); ok {
		n = x.(int)
	}
	m.Unlock()
	return n
}

// Delete deletes the value for the given points, if present. Returns the value (which will be 0 if the points were not in the counter).
func (S *Counter) Delete(pts Points) int {
	if S == nil {
		return 0
	}
	var n int
	i := int(pts.Hash()) % len(S.m)
	m := S.m[i]
	m.Lock()
	if x, ok := S.counts[i].Delete(pts); ok {
		n = x.(int)
		atomic.AddInt64(&S.n, -1)
	}
	m.Unlock()
	return n
}

// PointsWithValue returns a slice of all points with given value (which must be positive) at the time of call. The order of the points in the slice is random.
func (S *Counter) PointsWithValue(n int) []Points {
	if S == nil || n <= 0 {
		return nil
	}
	T := make([]Points, 0)
	for i, m := range S.m {
		m.Lock()
		S.counts[i].ForEach(func(pts Points, x interface{}) bool {
			if x.(int) == n {
				T = append(T, pts)
			}
			return true
		})
		m.Unlock()
	}
	rand.Shuffle(len(T), func(i int, j int) {
		T[i], T[j] = T[j], T[i]
	})
	return T
}

// ToSlice returns parallel slices of points and values. Only points whose value is >= n will be included. The order of the slices is random.
func (S *Counter) ToSlices(n int) ([]Points, []int) {
	if S == nil {
		return nil, nil
	}
	T := make([]Points, 0)
	C := make([]int, 0)
	for i, m := range S.m {
		m.Lock()
		S.counts[i].ForEach(func(pts Points, x interface{}) bool {
			if k := x.(int); k >= n {
				T = append(T, pts)
				C = append(C, k)
			}
			return true
		})
		m.Unlock()
	}
	rand.Shuffle(len(T), func(i int, j int) {
		T[i], T[j] = T[j], T[i]
		C[i], C[j] = C[j], C[i]
	})
	return T, C
}
