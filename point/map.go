package point

import (
	"bitbucket.org/pcastools/hash"
	"github.com/petar/GoLLRB/llrb"
	"sync"
	"sync/atomic"
)

// mapEntry is a key-value item, where the key is a slice of points.
type mapEntry struct {
	key   Points
	value interface{}
}

var mapEntryPool = sync.Pool{
	New: func() interface{} {
		return &mapEntry{}
	},
}

func newMapEntry(pts Points) *mapEntry {
	e := mapEntryPool.Get().(*mapEntry)
	e.key = pts
	return e
}

func reuseMapEntry(e *mapEntry) {
	e.key, e.value = nil, nil
	mapEntryPool.Put(e)
}

func (x *mapEntry) Less(than llrb.Item) bool {
	y := than.(*mapEntry)
	return pointsCmp(x.key, y.key) < 0
}

// Map defines a map from Points to data. Maps are not safe for concurrent modification.
type Map struct {
	n       int64                     // The size of the map
	buckets map[hash.Value]*llrb.LLRB // The underlying map
}

// Delete TODO
func (m *Map) Delete(pts Points) (value interface{}, ok bool) {
	// Is there anything to do?
	if m == nil || m.buckets == nil {
		return
	}
	// Calculate the hash and fetch the corresponding bucket
	h := pts.Hash()
	bucket, hasBucket := m.buckets[h]
	if !hasBucket {
		return
	}
	// Attempt to delete the entry from the bucket
	k := newMapEntry(pts)
	x := bucket.Delete(k)
	reuseMapEntry(k)
	// Was anything deleted?
	if x != nil {
		// Make a note of the value
		ok = true
		e := x.(*mapEntry)
		value = e.value
		reuseMapEntry(e)
		// Can we delete the bucket?
		if bucket.Len() == 0 {
			delete(m.buckets, h)
		}
		// Update the size
		atomic.AddInt64(&m.n, -1)
	}
	return
}

// Get TODO
func (m *Map) Get(pts Points) (value interface{}, ok bool) {
	// Is there anything to do?
	if m == nil || m.buckets == nil {
		return
	}
	// Calculate the hash and fetch the corresponding bucket
	bucket, hasBucket := m.buckets[pts.Hash()]
	if !hasBucket {
		return
	}
	// Attempt to get the entry from the bucket
	k := newMapEntry(pts)
	x := bucket.Get(k)
	reuseMapEntry(k)
	// Did we get anything? If so, make a note of the value
	if x != nil {
		ok = true
		e := x.(*mapEntry)
		value = e.value
	}
	return
}

// Set TODO
func (m *Map) Set(pts Points, value interface{}) (oldValue interface{}, ok bool) {
	// Ensure that the hash map exists
	if m.buckets == nil {
		m.buckets = make(map[hash.Value]*llrb.LLRB)
	}
	// Calculate the hash and fetch (or create) the corresponding bucket
	h := pts.Hash()
	bucket, hasBucket := m.buckets[h]
	if !hasBucket {
		bucket = llrb.New()
		m.buckets[h] = bucket
	}
	// Set the value
	k := newMapEntry(pts)
	k.value = value
	x := bucket.ReplaceOrInsert(k)
	// Was a previous value set?
	if x == nil {
		// Update the size
		atomic.AddInt64(&m.n, 1)
	} else {
		// Make a note of the old value
		ok = true
		e := x.(*mapEntry)
		oldValue = e.value
		reuseMapEntry(e)
	}
	return
}

// Has TODO
func (m *Map) Has(pts Points) bool {
	// Is there anything to do?
	if m == nil || m.buckets == nil {
		return false
	}
	// Calculate the hash and fetch the corresponding bucket
	bucket, hasBucket := m.buckets[pts.Hash()]
	if !hasBucket {
		return false
	}
	// Is the point in the bucket?
	k := newMapEntry(pts)
	ok := bucket.Has(k)
	reuseMapEntry(k)
	return ok
}

// ForEach TODO
func (m *Map) ForEach(f func(Points, interface{}) bool) {
	// Is there anything to do?
	if m == nil || m.buckets == nil {
		return
	}
	// Range over the hash buckets
	for _, bucket := range m.buckets {
		bucket.AscendGreaterOrEqual(bucket.Min(), func(x llrb.Item) bool {
			e := x.(*mapEntry)
			return f(e.key, e.value)
		})
	}
}

// Len TODO
func (m *Map) Len() int {
	if m == nil {
		return 0
	}
	return int(atomic.LoadInt64(&m.n))
}
