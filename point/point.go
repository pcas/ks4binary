package point

import (
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/stringsbuilder"
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
	"unicode"
)

// Point describes a 4-dimensional lattice point.
type Point [4]int16

// Points is a slice of points.
type Points []Point

// pointsCmp compares the two slices of points x and y. If len(x) < len(y) then x < y. If len(x) == len(y) then lexicographic order is used. Returns -1 if x < y, 0 if x == y, and +1 if x > y.
func pointsCmp(x Points, y Points) int {
	n := len(x)
	if m := len(y); n < m {
		return -1
	} else if m < n {
		return 1
	}
	for i := 0; i < n; i++ {
		if sgn := Cmp(x[i], y[i]); sgn != 0 {
			return sgn
		}
	}
	return 0
}

// Cmp compares the two points x and y using lexicographic order. We say that x > y if the left-most non-zero entry of x - y is positive. Returns -1 if x < y, 0 if x == y, and +1 if x > y.
func Cmp(x Point, y Point) int {
	for i := 0; i < 4; i++ {
		if x[i] < y[i] {
			return -1
		} else if x[i] > y[i] {
			return 1
		}
	}
	return 0
}

// Parse attents to convert the given string to a point. The string is expected to be in the format
//	[ c1, c2, c3, c4 ]
// where the white-space is optional and flexible.
func Parse(s string) (Point, error) {
	var pt Point
	s = strings.TrimSpace(s)
	n := len(s)
	if n < 2 || s[0] != '[' || s[n-1] != ']' {
		return pt, errors.New("invalid brackets")
	}
	S := strings.Split(s[1:n-1], ",")
	if len(S) != 4 {
		return pt, fmt.Errorf("illegal number of coefficients: %d", len(S))
	}
	for i, x := range S {
		c, err := strconv.ParseInt(strings.TrimSpace(x), 10, 64)
		if err != nil {
			return pt, fmt.Errorf("error parsing coefficient %d: %w", i+1, err)
		} else if c < math.MinInt16 || c > math.MaxInt16 {
			return pt, fmt.Errorf("coefficient %d is out of range", i+1)
		}
		pt[i] = int16(c)
	}
	return pt, nil
}

// Hash returns a hash value for the point.
func (pt Point) Hash() hash.Value {
	h := uint32(pt[0]) ^ uint32(pt[1])<<4 ^ uint32(pt[2])<<8 ^ uint32(pt[3])<<16
	return hash.Uint32(h)
}

// String returns a string description of the point. The string is formatted:
//	[c1,c2,c3,c4]
func (pt Point) String() string {
	b := stringsbuilder.New()
	b.WriteByte('[')
	for i := 0; i < 4; i++ {
		if i != 0 {
			b.WriteByte(',')
		}
		b.WriteString(strconv.Itoa(int(pt[i])))
	}
	b.WriteByte(']')
	s := b.String()
	stringsbuilder.Reuse(b)
	return s
}

// ParsePoints attents to convert the given string to a slice of points. The string is expected to be in the format
//	[ [ c1, c2, c3, c4 ], ... ]
// where the white-space is optional and flexible.
func ParsePoints(s string) (Points, error) {
	s = strings.TrimSpace(s)
	n := len(s)
	if n < 2 || s[0] != '[' || s[n-1] != ']' {
		return nil, errors.New("invalid brackets")
	}
	s = strings.TrimSpace(s[1 : n-1])
	pts := make(Points, 0)
	for len(s) != 0 {
		idx := strings.Index(s, "]")
		if idx == -1 {
			return nil, errors.New("malformed string")
		}
		// Parse the point
		pt, err := Parse(s[:idx+1])
		if err != nil {
			return nil, fmt.Errorf("error parsing point %d: %w", len(pts)+1, err)
		}
		pts = append(pts, pt)
		// Move on
		if idx == len(s)-1 {
			s = ""
		} else {
			s = strings.TrimLeftFunc(s[idx+1:], unicode.IsSpace)
			if len(s) < 2 || s[0] != ',' {
				return nil, errors.New("malformed string")
			}
			s = s[1:]
		}
	}
	return pts, nil
}

// Hash returns a hash value for the slice of points.
func (pts Points) Hash() hash.Value {
	n := len(pts)
	if n == 0 {
		return 0
	}
	h := pts[0].Hash()
	for i := 1; i < n; i++ {
		h = hash.Combine(h, pts[i].Hash())
	}
	return h
}

// Equals returns true iff pts equals x.
func (pts Points) Equals(x Points) bool {
	n := len(pts)
	if len(x) != n {
		return false
	}
	for i := range pts {
		if pts[i] != x[i] {
			return false
		}
	}
	return true
}

// String returns a string description of the points. The string is formatted:
//	[[c1,c2,c3,c4],...]
func (pts Points) String() string {
	b := stringsbuilder.New()
	b.WriteByte('[')
	for i, pt := range pts {
		if i != 0 {
			b.WriteByte(',')
		}
		b.WriteByte('[')
		for j := 0; j < 4; j++ {
			if j != 0 {
				b.WriteByte(',')
			}
			b.WriteString(strconv.Itoa(int(pt[j])))
		}
		b.WriteByte(']')
	}
	b.WriteByte(']')
	s := b.String()
	stringsbuilder.Reuse(b)
	return s
}
