// Dict provides a point dictionary.

package point

import (
	"bitbucket.org/pcastools/bytesbuffer"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math"
	"sync"
	"sync/atomic"
)

// bufPool is a pool of buffers used for encoding.
var bufPool = &sync.Pool{
	New: func() interface{} {
		return make([]byte, binary.MaxVarintLen64)
	},
}

// Dict is a dictionary used when encoding/decoding points.
type Dict struct {
	hasEncode int64      // Have we built the encode data? Note this needs to be at the top of the struct to ensure alignment for atomic.
	decode    *decodeMap // The decode map
	m         sync.Mutex // Protects the following
	encode    *encodeMap // The encode map
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// decodeRemainderOfPoint decodes a point from r with initial coefficient is c.
func decodeRemainderOfPoint(r io.ByteReader, c int16) (Point, error) {
	pt := Point{}
	pt[0] = c
	for i := 1; i < 4; i++ {
		n, err := binary.ReadVarint(r)
		if err != nil {
			return Point{}, fmt.Errorf("error reading coefficient %d: %w", i+1, err)
		} else if n < math.MinInt16 || n > math.MaxInt16 {
			return Point{}, fmt.Errorf("error reading coefficient %d: coefficient out of range", i+1)
		}
		pt[i] = int16(n)
	}
	return pt, nil
}

/////////////////////////////////////////////////////////////////////////
// Dict functions
/////////////////////////////////////////////////////////////////////////

// Len returns the size of the dictionary.
func (d *Dict) Len() int {
	if d == nil {
		return 0
	}
	return d.decode.Len()
}

// buildEncodeDict ensures that the encode dictionary has been built.
func (d *Dict) buildEncodeDict() {
	// Is there anything to do?
	if d == nil || atomic.LoadInt64(&d.hasEncode) != 0 {
		return
	}
	// Acquire a lock and check whether the dictionary was built whilst waiting
	d.m.Lock()
	defer d.m.Unlock()
	if atomic.LoadInt64(&d.hasEncode) != 0 {
		return
	}
	// Create the encode map and mark the encode dictionary as built
	d.encode = newEncodeMap(d.decode)
	atomic.StoreInt64(&d.hasEncode, 1)
}

// scanPointsForID will scan the points with given ID. The second return value is true iff the ID exists in the dictionary. If true, also returns the number of points scanned, or -1 if the length was too short to hold the points.
func (d *Dict) scanPointsForID(id int64, pts Points) (int, bool) {
	if d == nil {
		return 0, false
	}
	return d.decode.Scan(id, pts)
}

// PointsForID returns the points for the given ID, if known. The second return value is true iff the ID exists in the dictionary.
func (d *Dict) PointsForID(id int64) (Points, bool) {
	if d == nil {
		return nil, false
	}
	return d.decode.Entry(id)
}

// lookupIDForPoints returns the ID for the given points, if known. The second return value is true iff the points exists in the dictionary. Assumes that the encode dictionary has been built.
func (d *Dict) lookupIDForPoints(pts Points) (int64, bool) {
	if d == nil {
		return 0, false
	}
	return d.encode.Entry(pts)
}

// IDForPoints returns the ID for the given points, if known. The second return value is true iff the points exists in the dictionary.
func (d *Dict) IDForPoints(pts Points) (int64, bool) {
	d.buildEncodeDict()
	return d.lookupIDForPoints(pts)
}

// lookupIDForPoint returns the ID for the given point, if known. The second return value is true iff the point exists in the dictionary. If true, also returns the ID. Assumes that the encode dictionary has been built.
func (d *Dict) lookupIDForPoint(pt Point) (int64, bool) {
	return d.lookupIDForPoints(Points{pt})
}

// decodePointsNoLength decodes a slice of points from r of length n.
func (d *Dict) decodePointsNoLength(r io.ByteReader, n int) (Points, error) {
	// Read in the points
	pts := make(Points, n)
	for i := 0; i < n; {
		// The first value determines whether to consult the dictionary
		if c, err := binary.ReadVarint(r); err != nil {
			return nil, fmt.Errorf("error reading point %d: %w", i+1, err)
		} else if c >= minDictID {
			k, ok := d.scanPointsForID(c, pts[i:])
			if !ok {
				return nil, fmt.Errorf("error reading point %d: unknown dictionary ID: %d", i+1, c)
			} else if k == -1 {
				return nil, errors.New("corrupt or incorrect dictionary: decoded too many points")
			}
			i += k
		} else {
			// Sanity check on the size of c
			if c < math.MinInt16 || c > math.MaxInt16 {
				return nil, fmt.Errorf("error reading point %d: error reading coefficient 1: coefficient out of range", i+1)
			}
			// This isn't a dictionary entry
			pt, err := decodeRemainderOfPoint(r, int16(c))
			if err != nil {
				return nil, fmt.Errorf("error reading point %d: %w", i+1, err)
			}
			pts[i] = pt
			i++
		}
	}
	// Return the points
	return pts, nil
}

// DecodePoints decodes a slice of points from r.
func (d *Dict) DecodePoints(r io.ByteReader) (Points, error) {
	// The first value is the number of points
	n, err := binary.ReadVarint(r)
	if err != nil {
		if err != io.EOF {
			err = fmt.Errorf("error reading number of points: %w", err)
		}
		return nil, err
	} else if n < 0 || n > 1000 {
		return nil, fmt.Errorf("invalid number of points: %d", n)
	}
	// Read in the points
	return d.decodePointsNoLength(r, int(n))
}

// encodeSuccessivePointsOfLength encodes as many successive slices of k points from pts, writing to w, using the given buffer. Returns the unencoded portion of pts, and the number of bytes written. Assumes that the encode dictionary has been built.
func (d *Dict) encodeSuccessivePointsOfLength(pts Points, w io.Writer, buf []byte, k int) (Points, int, error) {
	// Keep track of the number of bytes of output
	var n int
	// Output the points
	for len(pts) >= k {
		// Look up the ID for the next slice of k point
		id, ok := d.lookupIDForPoints(pts[:k])
		if !ok {
			return pts, n, nil
		}
		// Write out the id
		m := binary.PutVarint(buf, id)
		m, err := w.Write(buf[:m])
		n += m
		if err != nil {
			return pts, n, err
		}
		// Move on
		pts = pts[k:]
	}
	return pts, n, nil
}

// encodePoint encodes a point, writing to w, using the given buffer. The encoding dictionary will be used if useDict it true. Returns the number of bytes written. Assumes that the encode dictionary has been built.
func (d *Dict) encodePoint(pt Point, w io.Writer, buf []byte, useDict bool) (int, error) {
	// Is the point in the dictionary?
	if useDict {
		id, ok := d.lookupIDForPoint(pt)
		if ok {
			// Write out the id and return
			n := binary.PutVarint(buf, id)
			n, err := w.Write(buf[:n])
			if err != nil {
				return n, fmt.Errorf("error writing ID: %w", err)
			}
			return n, nil
		}
	}
	// The initial coefficient must be less than minDictID
	if pt[0] >= minDictID {
		return 0, fmt.Errorf("coefficient 1 (%d) too lage", pt[0])
	}
	// Keep track of the number of bytes of output
	var n int
	// Write out the coefficients
	for i := 0; i < 4; i++ {
		m := binary.PutVarint(buf, int64(pt[i]))
		m, err := w.Write(buf[:m])
		n += m
		if err != nil {
			return n, fmt.Errorf("error writing coefficient %d: %w", i+1, err)
		}
	}
	return n, nil
}

// encodePointsNoLength encodes a slice of points, writing to w, using the given buffer. Does not write out the length of the slice of points. Encoding dictionaries up to and including length k are used. Returns the number of bytes written. Assumes that the encode dictionary has been built.
func (d *Dict) encodePointsNoLength(pts Points, w io.Writer, buf []byte, k int) (int, error) {
	// Keep track of the number of bytes of output
	var n int
	// Encode as many consecutive slices of k points as possible
	for i := k; i >= 2; i-- {
		var m int
		var err error
		pts, m, err = d.encodeSuccessivePointsOfLength(pts, w, buf, i)
		n += m
		if err != nil {
			return n, fmt.Errorf("error writing points: %w", err)
		}
	}
	// Encode the remaining points
	useDict := k >= 1
	for i := range pts {
		m, err := d.encodePoint(pts[i], w, buf, useDict)
		n += m
		if err != nil {
			return n, fmt.Errorf("error writing points: %w", err)
		}
	}
	return n, nil
}

// encodePoints encodes a slice of points, writing to w, using the given buffer. Encoding dictionaries up to and including length k are used. Returns the number of bytes written. Assumes that the encode dictionary has been built.
func (d *Dict) encodePoints(pts Points, w io.Writer, buf []byte, k int) (int, error) {
	// Keep track of the number of bytes of output
	var n int
	// Sanity check
	size := len(pts)
	if size > 1000 {
		return n, fmt.Errorf("too many points in point slice (%d)", size)
	}
	// First we write out the number of points
	m := binary.PutVarint(buf, int64(size))
	m, err := w.Write(buf[:m])
	n += m
	if err != nil {
		return n, fmt.Errorf("error writing number of points: %w", err)
	}
	// Now write out the points
	m, err = d.encodePointsNoLength(pts, w, buf, k)
	n += m
	return n, err
}

// EncodePoints encodes a slice of points, writing to w. Returns the number of bytes written.
func (d *Dict) EncodePoints(pts Points, w io.Writer) (int, error) {
	// Ensure that the encode dicts have been built
	d.buildEncodeDict()
	// Fetch a buffer and encode the points
	buf := bufPool.Get().([]byte)
	n, err := d.encodePoints(pts, w, buf, len(pts))
	bufPool.Put(buf)
	return n, err
}

// writeDecodeDict writes the decode dictionary points of length k to w, using the given buffer. Returns the number of bytes written, and the number of points read. Assumes that the encode dictionary has been built.
func (d *Dict) writeDecodeDict(w io.Writer, k int, buf []byte) (int64, int, error) {
	// Fetch a bytes buffer
	b := bytesbuffer.New()
	defer bytesbuffer.Reuse(b)
	// Keep track of the number of points written
	var num int
	// Start working through the points
	for _, pts := range d.decode.PointsOfLen(k) {
		// Write out the point to b
		var err error
		if k == 1 {
			_, err = d.encodePoint(pts[0], b, buf, false)
		} else {
			_, err = d.encodePointsNoLength(pts[:k], b, buf, k-1)
		}
		if err != nil {
			return 0, 0, fmt.Errorf("error writing decode dict points: %w", err)
		}
		num++
	}
	// Keep track of the number of bytes written
	var n int64
	// Write out the number of points
	m := binary.PutVarint(buf, int64(num))
	m, err := w.Write(buf[:m])
	n += int64(m)
	if err != nil {
		return n, num, fmt.Errorf("error writing decode dict size: %w", err)
	}
	// Write out the points
	l, err := b.WriteTo(w)
	n += l
	if err != nil {
		return n, num, fmt.Errorf("error writing decode dict data: %w", err)
	}
	return n, num, nil
}

// WriteTo writes the dictionary to w. Returns the number of bytes written.
func (d *Dict) WriteTo(w io.Writer) (int64, error) {
	// Fetch a buffer
	buf := bufPool.Get().([]byte)
	defer bufPool.Put(buf)
	// Ensure that the encode dicts have been built
	d.buildEncodeDict()
	// Keep track of the total number of bytes written
	var n int64
	// Work through the points in the dictionary by length
	N := d.Len()
	for k := 1; N > 0; k++ {
		m, num, err := d.writeDecodeDict(w, k, buf)
		n += m
		N -= num
		if err != nil {
			return n, err
		}
	}
	// Write out -1, indicating the end of the dictionary
	m := binary.PutVarint(buf, -1)
	m, err := w.Write(buf[:m])
	n += int64(m)
	if err != nil {
		return n, fmt.Errorf("error writing decode dict: %w", err)
	}
	return n, nil
}

// Append returns a new dictionary with the given points added. Assumes that the points in pts are distinct.
func (d *Dict) Append(pts ...Points) (*Dict, error) {
	// Is there anything to do?
	if len(pts) == 0 {
		return d, nil
	}
	// Ensure that the encode dicts have been built
	d.buildEncodeDict()
	// Validate the points
	for i, p := range pts {
		if n := len(p); n == 0 {
			return nil, fmt.Errorf("invalid points at index %d: length must be non-zero", i)
		} else if _, ok := d.lookupIDForPoints(p); ok {
			return nil, fmt.Errorf("points at index %d already in the dictionary", i)
		}
	}
	// Add the points to a copy of this dictionary
	d2 := &Dict{}
	if d == nil {
		d2.decode = (&decodeMap{}).Append(pts...)
	} else {
		d2.decode = d.decode.Append(pts...)
	}
	return d2, nil
}

// readDecodeDict reads in the points for the next block in the decode dictionary, which is assumed to be points of length n. Returns io.EOF if the end of the dictionary has been reached.
func (d *Dict) readDecodeDict(r io.ByteReader, n int) ([]Points, error) {
	// The first value is the number of points in this block
	m, err := binary.ReadVarint(r)
	if err != nil {
		return nil, fmt.Errorf("error reading decode dictionary block size: %w", err)
	} else if m < 0 {
		if m == -1 {
			return nil, io.EOF
		}
		return nil, fmt.Errorf("invalid decode dictionary block size (%d)", m)
	}
	num := int(m)
	// Read in the points
	pts := make([]Points, 0, num)
	for i := 0; i < num; i++ {
		p, err := d.decodePointsNoLength(r, n)
		if err != nil {
			return nil, fmt.Errorf("error reading point %d from block %d in decode dictionary: %w", i+1, n, err)
		}
		pts = append(pts, p)
	}
	return pts, nil
}

// ReadDict reads a dictionary from r.
func ReadDict(r io.ByteReader) (*Dict, error) {
	// Create a new dictionary
	d := &Dict{}
	// Start working through the points
	for i := 1; ; i++ {
		// Read in the next block of points
		pts, err := d.readDecodeDict(r, i)
		if err != nil {
			if err == io.EOF {
				return d, nil
			}
			return nil, err
		}
		// Add the points to the dictionary
		d.decode = d.decode.Set(i, pts)
	}
}
