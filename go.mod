module bitbucket.org/pcas/ks4binary

go 1.14

require (
	bitbucket.org/pcastools/bytesbuffer v1.0.1
	bitbucket.org/pcastools/flag v0.0.11
	bitbucket.org/pcastools/hash v1.0.2
	bitbucket.org/pcastools/stringsbuilder v1.0.1
	github.com/c2h5oh/datasize v0.0.0-20200112174442-28bbd4740fee
	github.com/gosuri/uilive v0.0.4 // indirect
	github.com/gosuri/uiprogress v0.0.1
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/olekukonko/tablewriter v0.0.4
	github.com/petar/GoLLRB v0.0.0-20190514000832-33fb24c13b99
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
)
